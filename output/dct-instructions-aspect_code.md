### Instruction for coding aspects
*This overview was generated on 2020-04-10 at 16:21:29 CEST (GMT+0200)*

####   Actual control
*When coding aspects, use the following code: **`dct:actualControl`***

*(Not specified)*

####   Attitude
*When coding aspects, use the following code: **`dct:attitude_73dnt5zc`***

Any global evaluation of the target behavior as a whole. Note that evaluations of specific aspects or consequences of the behavior relate to aspects of underlying constructs, not of attitude itself. Also make sure to check the coding instructions for the two sub-constructs that attitude consists of: [dct:experientialAttitude_73dnt5z5](#experientialAttitude_73dnt5z5) and [dct:instrumentalAttitude_73dnt5zb](#instrumentalAttitude_73dnt5zb).

####   Autonomy
*When coding aspects, use the following code: **`dct:autonomy_73dnt5zx`***

*(Not specified)*

####   Autonomy belief
*When coding aspects, use the following code: **`dct:autonomy_belief_73dnt5zt`***

*(Not specified)*

####   Autonomy beliefs composite
*When coding aspects, use the following code: **`dct:autonomy_belCom_73dnt5zw`***

*(Not specified)*

####   Capacity
*When coding aspects, use the following code: **`dct:capacity_73dnt602`***

Expressions that demonstrate or imply the presence or absence of confidence about one's *ability* to perform the target behavior. Expressions that refer to explicit control over the target behavior (or lack thereof), for example because of a barrier, obstacle, or facilitating condition or circumstance, it should be coded as [dct:perceivedBehavioralControl_autonomy_73bg9sq6](#perceivedBehavioralControl_autonomy_73bg9sq6). Expressions that refer to confidence in general (not in one's *ability*), should be coded as [dct:perceivedBehavioralControl_71w8sfdk](#perceivedBehavioralControl_71w8sfdk).

####   Capacity belief
*When coding aspects, use the following code: **`dct:capacity_belief_73dnt600`***

*(Not specified)*

####   Capacity beliefs composite
*When coding aspects, use the following code: **`dct:capacity_belCom_73dnt601`***

*(Not specified)*

####   Descriptive norm belief
*When coding aspects, use the following code: **`dct:descrNorms_belief_73dnt5zm`***

*(Not specified)*

####   Descriptive norms
*When coding aspects, use the following code: **`dct:descriptiveNorms_73dnt5zp`***

Expressions of perceived behavior of others, i.e. whether others perform the target behavior or not. Note that exressions of perceived *approval or disapproval* should be coded as [dct:perceivedNorm_injunctive_73bg2wm7](#perceivedNorm_injunctive_73bg2wm7), and expressions of perceived norms where it is unclear whether they concern perceived (dis)approval or perceived approval should be coded as [dct:perceivedNorm_71w98kk2](#perceivedNorm_71w98kk2).

####   Descriptive norms belief composite
*When coding aspects, use the following code: **`dct:descrNorms_belCom_73dnt5zn`***

*(Not specified)*

####   Experiential attitude
*When coding aspects, use the following code: **`dct:experientialAttitude_73dnt5z5`***

*(Not specified)*

####   Experiential attitude belief
*When coding aspects, use the following code: **`dct:expAttitude_belief_73dnt5z3`***

*(Not specified)*

####   Experiential attitude belief evaluation
*When coding aspects, use the following code: **`dct:expAttitude_evaluation_73dnt5z2`***

Expressions of one's evaluations as positive or negative (i.e. desirable or undesirable) of experiential potential consequences of a behavior. The experiential nature of this evaluation means that this these evaluations concern mostly expected experiences and sensations, such as pleasure or pain.

Note that expressions of evaluations of consequences that render the target behavior less or more instrumental given more long-term goals are captured in instrumental attitude and the underlying beliefs (see [dct:instrAttitude_evaluation_73dnt5z7](#instrAttitude_evaluation_73dnt5z7)).

One's actual expectation (i.e. whether a consequence is likely or unlikely) should be coded as [dct:expAttitude_expectation_73dnt5z1](#expAttitude_expectation_73dnt5z1).

####   Experiential attitude belief expectation
*When coding aspects, use the following code: **`dct:expAttitude_expectation_73dnt5z1`***

Expressions of expectations of how probable (i.e. unlikely versus likely) it is that engaging in the target behavior will cause an experiential potential consequence to come about. The experiential nature of this expectation means that these expected consequences must concern experiences and sensations, such as pleasure or pain. These experiential attitude belief expectations cover acute hedonic expectations, disconnected from potential long-term consequences the target behavior may have. Experiential attitude belief expectations always refer to immediately experienced consequences of a behavior.

Note that expressions of expectations of consequences that render the target behavior less or more desirable without the immediate experiential effects of the behavior playing a role are captured in instrumental attitude belief expectations (see [dct:instrAttitude_expectation_73dnt5z6](#instrAttitude_expectation_73dnt5z6)).

One's evaluation in terms of valence (i.e. positive versus negative) of the consequence should be coded as [dct:expAttitude_evaluation_73dnt5z2](#expAttitude_evaluation_73dnt5z2).

####   Experiential attitude beliefs composite
*When coding aspects, use the following code: **`dct:expAttitude_belCom_73dnt5z4`***

*(Not specified)*

####   Identification with referent
*When coding aspects, use the following code: **`dct:referentIdentification_73dnt5zl`***

*(Not specified)*

####   Injunctive norm belief
*When coding aspects, use the following code: **`dct:injNorms_belief_73dnt5zg`***

*(Not specified)*

####   Injunctive norm beliefs composite
*When coding aspects, use the following code: **`dct:injNorms_belCom_73dnt5zh`***

*(Not specified)*

####   Injunctive norms
*When coding aspects, use the following code: **`dct:injunctiveNorms_73dnt5zj`***

*(Not specified)*

####   Instrumental attitude
*When coding aspects, use the following code: **`dct:instrumentalAttitude_73dnt5zb`***

*(Not specified)*

####   Instrumental attitude belief
*When coding aspects, use the following code: **`dct:instrAttitude_belief_73dnt5z8`***

*(Not specified)*

####   Instrumental attitude belief evaluation
*When coding aspects, use the following code: **`dct:instrAttitude_evaluation_73dnt5z7`***

Expressions of one's evaluations as positive or negative (i.e. desirable or undesirable) of instrumental potential consequences of a behavior. The instrumental nature of this evaluation means that this these evaluations contain information about people's longer term goals.

Note that expressions of evaluations of consequences that concern experiences and sensations, such as pleasure or pain, are captured in experiential attitude and the underlying beliefs (see [dct:expAttitude_evaluation_73dnt5z2](#expAttitude_evaluation_73dnt5z2)).

One's actual expectation (i.e. whether a consequence is likely or unlikely) should be coded as [dct:instrAttitude_expectation_73dnt5z6](#instrAttitude_expectation_73dnt5z6).

####   Instrumental attitude belief expectation
*When coding aspects, use the following code: **`dct:instrAttitude_expectation_73dnt5z6`***

Expressions of expectations of how probable (i.e. unlikely versus likely) it is that engaging in the target behavior will cause an instrumental potential consequence to come about. The instrumental nature of this expectation means that these expected consequences must concern facilitation or obstruction of achieving one or more longer term goals. These instrumental attitude belief expectations cover expectations of potential long-term consequences, disconnected from acute hedonic consequences the target behavior may have. Instrumental attitude belief expectations always refer to assumed benefits or costs of a behavior.

Note that expressions of expectations of consequences that render the target behavior less or more desirable where the immediate experiential effects of the behavior play a role are captured in experiential attitude belief expectations (see [dct:expAttitude_expectation_73dnt5z1](#expAttitude_expectation_73dnt5z1)).

One's evaluation in terms of valence (i.e. positive versus negative) of the consequence should be coded as [dct:instrAttitude_evaluation_73dnt5z7](#instrAttitude_evaluation_73dnt5z7).

####   Instrumental attitude beliefs composite
*When coding aspects, use the following code: **`dct:instrAttitude_belCom_73dnt5z9`***

*(Not specified)*

####   Intention
*When coding aspects, use the following code: **`dct:intention_73dnt604`***

Any expressions that somebody has the intention, goal, or plan to perform a target behavior.

####   Motivation to comply
*When coding aspects, use the following code: **`dct:motivationToComply_73dnt5zf`***

*(Not specified)*

####   Perceived behavioral control
*When coding aspects, use the following code: **`dct:perceivedBehavioralControl_73dnt603`***

*(Not specified)*

####   Perceived norms
*When coding aspects, use the following code: **`dct:perceivedNorms_73dnt5zq`***

Expressions that refer to social pressure *in general*. However, expressions about perceived behavior of others are coded as [dct:perceivedNorm_descriptive_73bg61tx](#perceivedNorm_descriptive_73bg61tx) and expressions about perceived (dis)approval of others are coded as [dct:perceivedNorm_injunctive_73bg2wm7](#perceivedNorm_injunctive_73bg2wm7).

####   Perceived power of condition
*When coding aspects, use the following code: **`dct:autonomy_conditionPower_73dnt5zs`***

*(Not specified)*

####   Perceived presence of condition
*When coding aspects, use the following code: **`dct:autonomy_conditionPresence_73dnt5zr`***

*(Not specified)*

####   Perceived referent approval
*When coding aspects, use the following code: **`dct:referentApproval_73dnt5zd`***

*(Not specified)*

####   Perceived referent behavior
*When coding aspects, use the following code: **`dct:referentBehavior_73dnt5zk`***

*(Not specified)*

####   Perceived subskill importance
*When coding aspects, use the following code: **`dct:capacity_subskillImportance_73dnt5zz`***

*(Not specified)*

####   Perceived subskill presence
*When coding aspects, use the following code: **`dct:capacity_subskillPresence_73dnt5zy`***

*(Not specified)*

####   Target behavior
*When coding aspects, use the following code: **`dct:behavior_73dnt605`***

*(Not specified)*

