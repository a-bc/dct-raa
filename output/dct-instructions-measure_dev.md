### Instruction for developing measurement instruments
*This overview was generated on 2020-04-10 at 16:21:29 CEST (GMT+0200)*

####   Actual control

*(Not specified)*

####   Attitude

Attitude consists of two sub-constructs, and is measured by measuring those: [dct:experientialAttitude_73dnt5z5](#experientialAttitude_73dnt5z5) and [dct:instrumentalAttitude_73dnt5zb](#instrumentalAttitude_73dnt5zb).

####   Autonomy

*(Not specified)*

####   Autonomy belief

*(Not specified)*

####   Autonomy beliefs composite

*(Not specified)*

####   Capacity

Use Likert scales that measure the degree to which participants believe the target behavior to be something they are capable of performing successfully. The items suggested in the book are: 'I am confident that if I want to, I can [TARGET BEHAVIOR].' with anchors 'No confidence at all' vs 'A lot of confidence' and 'If I really wanted to, I could [TARGET BEHAVIOR].' with anchors 'Unlikely' vs 'Likely'. Note that questions about how easy or difficult participants perceive a target behavior to be fall outside of the constructs of Perceived Behavioral Control, Autonomy, and Capacity as defined in the 2010 RAA book.

####   Capacity belief

*(Not specified)*

####   Capacity beliefs composite

*(Not specified)*

####   Descriptive norm belief

*(Not specified)*

####   Descriptive norms

Use likert scales that measure perceived behavior of important social referents in general. It is important that no specific individuals are referenced, but that the items do refer to individuals that are similar, important or seen as important to follow. Note that the behavior in these items must always be the target behavior. The example items in the 2010 RAA book are 'Most people I respect and admire will [TARGET BEHAVIOR].' with anchors 'Unlikely' vs 'Likely'; and 'How many people like you [TARGET BEHAVIOR]?' with anchors 'Nobody' vs 'Everybody'.

####   Descriptive norms belief composite

*(Not specified)*

####   Experiential attitude

Use semantic differentials with root 'For me, [TARGET BEHAVIOR] is ...' and a bidimensional scale where the right-most anchor expresses a pleasant affective state and the left-most anchor expresses the opposite unpleasant affective state (e.g. 'unpleasant' versus 'pleasant'). The example items in the 2010 RAA book are 'For me, [TARGET BEHAVIOR] is ...' with anchors 'Bad' vs 'Good'; 'For me, [TARGET BEHAVIOR] is ...' with anchors 'Unpleasant' vs 'Pleasant'; 'For me, [TARGET BEHAVIOR] is ...' with anchors 'Harmful' vs 'Beneficial'; and 'For me, [TARGET BEHAVIOR] is ...' with anchors 'Boring' vs 'Interesting'.

####   Experiential attitude belief

Experiential attitude beliefs are measured by measuring both of the component constructs: [dct:expAttitude_expectation_73dnt5z1](#expAttitude_expectation_73dnt5z1) and [dct:expAttitude_evaluation_73dnt5z2](#expAttitude_evaluation_73dnt5z2).

####   Experiential attitude belief evaluation

Experiential attitude belief evaluations are measured using questions in a questionnaire that are referred to as 'items'. Experiential attitude belief evaluation always relate to specific experiential attitude belief expectations, so the first step is to identify the expectation for which the evaluation should be measured (e.g. 'Drinking alcohol makes me feel ... [much more relaxed|much more excited]'). Evaluations of expectations are always measured on a bidimensional scale.

As item stem, use 'I prefer ...', and as anchors, include the scale extremes used when measuring the expectation. For example, an item could be 'I prefer ... [being much more relaxed|being much more excited]'. Make sure to formulate the item stem such that it makes clear that you are asking people about *their* evaluation (not what they think holds more generally). Use a seven-point response scale and try to always be consistent in the scale valence; in languages that are read from left to right, always place the most passive/low/less/weak/unlikely scale extreme (anchor) on the left, and the most active/high/more/strong/likely scale extreme (anchor) on the right. Do not reverse this order for one or more items.

The item or set of items should be accompanied by an instruction that makes clear that you are asking people about *their* evaluation (not what they think holds more generally).

####   Experiential attitude belief expectation

Experiential attitude belief expectations are measured using questions in a questionnaire that are referred to as 'items'. To measure an experiential attitude belief expectation, first, identify exactly which potential experiential consequence of the target behavior (e.g. a specific experience or sensation) you want to measure. Then, establish whether accurately describing the spectrum of possibilities regarding this experiential consequence requires a unidimensional scale or a bidimensional scale. Most potential consequences of a target behavior can be perceived either to *increase* or *decrease* upon performance of the target behavior, requiring a bidimensional scale. For example, some people might expect they will feel *less* relaxed if they drink alcohol, while others might expect they will feel *more* relaxed. However, in rare cases, one of the two dimensions can be excluded a priori, in which case a unidimensional scale suffices. For example, the degree to which people will expect they will feel less hungry may vary; but it is excessively unlikely that somebody might expect that if they eat a whole pizza, they will then feel *more* hungry. Therefore, in that case, one might want to choose a unidimensional scale. In general, a rule of thumb is that if the 'default state' of this experiential consequence resembles absence of the experience, and therefore, engaging in the target behavior can only have an effect in one direction, a unidimensional scale can be used. However, when engaging in the target behavior can conceivably increase *or* decrease this experiential consequence, a bidimensional scale is required.

Once it is clear whether a bidimensional or unidimensional scale should be used, the construction of the item stem and the two anchors can start. Make sure to formulate the item stem such that it makes clear that you are asking people about *their* expectation (not what they think holds more generally).

For unidimensional scales, create an item stem that explicitly lists the single dimension that expresses the experiential consequence (e.g. feeling full after eating a pizza). As anchors, always use 'Very unlikely' and 'Very likely'. An example item would be: 'If I eat a whole pizza at once, it is ... that it will make me feel full. [Very unlikely|Very likely]'. However, as explained above, unidimensional scales can rarely be used in most circumstances, so bidimensional scales are usually required.

For bidimensional scales, the item stem cannot explicitly list only one of the two dimensions (e.g. 'feeling much less relaxed' or 'feeling much more relaxed'). This is because this would create a unidimensional or ambiguous response scale. People who would score low on the unidimensional scale might mean either that they don't think that consequence will occur, or that they think that the opposite consequence will occur. For example, when creating an item 'If I drink a glass of alcohol, I will feel much more relaxed. [Very unlikely|Very likely]', people who respond 'Very unlikely' can mean either that they expect to feel much less relaxed, or that they expect that drinking a glass of alcohol will have no effect on how relaxed they will feel.

Therefore, capturing the full potential breadth of the beliefs of your target population requires asking what they expect exactly. To do this, create an item stem that contains the target behavior, and anchors that express the extremes of the bidimensional scale. For example, 'Drinking alcohol makes me feel ... [much less relaxed|much more relaxed]'. Sometimes, the two extremes of the dimension you want to measure can be expressed in two antonyms, such as 'Drinking alcohol makes me feel ... [much more relaxed|much more excited]'.

Once the item stem and the two anchors have been determined, decide which response scale to use. For bidimensional scales, seven-point scales are preferred, as these leave three degrees of expression in each dimension (the mid-point representing the expectation that the behavior does not have a consequence regarding this specific experiential attitude belief). For unidimensional scales, five-point scales suffice. Try to always be consistent in the scale valence; in languages that are read from left to right, always place the most passive/low/less/weak/unlikely scale extreme (anchor) on the left, and the most active/high/more/strong/likely scale extreme (anchor) on the right. Do not reverse this order for one or more items.

When combining multiple items in one measurement instrument, if both evaluations of unidimensional consequences and evaluations of bidimensional consequences are measured, either use two matrices or combine them in one that uses seven-point scales for all items.

The item or set of items should be accompanied by an instruction that makes clear that you are asking people about *their* expectation (not what they think holds more generally).

####   Experiential attitude beliefs composite

*(Not specified)*

####   Identification with referent

To measure the identification with a given social referent, measure how much people want to be like the relevant social referents with regard to the target behavior.

Note that for any given social referent, it's possible that an individual wants to be like that referent, but it's also possible that the individual actively wants to be *unlike* that social referent. For example, people may strongly want to distance themselves from (be unlike) members of a perceived outgroup (e.g. adolescents may want to be unlike middle-aged people).

Therefore, bidimensional scales are required to account for this variation. As item stem, use 'Concerning [BEHAVIOR], I want to be like [SOCIAL REFERENT]...', with anchors 'as little as possible' versus 'as much as possible'. For example, 'Concerning being able to speak in public, I want to be like a movie star... [As little as possible|As much as possible]'

Because this is a bidimensional scale, the scale midpoint reflects neutrality (i.e. the participant neither wants to identify with, nor distance themselves from, the relevant social referent regarding this target behavior). This means that a seven-point response scale is slightly preferred over a five-point response scale to allow participants to express three degrees of desire to be similar or dissimilar.

####   Injunctive norm belief

*(Not specified)*

####   Injunctive norm beliefs composite

*(Not specified)*

####   Injunctive norms

Use likert scales that measure both perceived social approval and perceived behavior of important social referents in general. It is important that no specific individuals are referenced, but that the items do refer to individuals that are similar, important or seen as important to follow. Note that the behavior in these items must always be the target behavior. The example items in the 2010 RAA book are 'Most people who are important to me think I should [TARGET BEHAVIOR].' with anchors 'False' vs 'True'; 'Most people whose opinions I value would ... of my [TARGET BEHAVIOR]' with anchors 'Disapprove' vs 'Approve'; 'Most people I respect and admire will [TARGET BEHAVIOR].' with anchors 'Unlikely' vs 'Likely'; and 'How many people like you [TARGET BEHAVIOR]?' with anchors 'Nobody' vs 'Everybody'.

####   Instrumental attitude

Use semantic differentials with root 'For me, [TARGET BEHAVIOR] is ...' and a bidimensional scale where the right-most anchor expresses a generally desirable instrumental state/goal and the left-most anchor expresses the opposite undesirable state/goal (e.g. 'unwise' versus 'wise' and 'bad' versus 'good'). The example items in the 2010 RAA book are 'For me, [TARGET BEHAVIOR] is ...' with anchors 'Bad' vs 'Good'; 'For me, [TARGET BEHAVIOR] is ...' with anchors 'Unpleasant' vs 'Pleasant'; 'For me, [TARGET BEHAVIOR] is ...' with anchors 'Harmful' vs 'Beneficial'; and 'For me, [TARGET BEHAVIOR] is ...' with anchors 'Boring' vs 'Interesting'.

####   Instrumental attitude belief

*(Not specified)*

####   Instrumental attitude belief evaluation

Instrumental attitude belief evaluations are measured using questions in a questionnaire that are referred to as 'items'. Instrumental attitude belief evaluation always relate to specific instrumental attitude belief expectations, so the first step is to identify the expectation for which the evaluation should be measured (e.g. 'Drinking four cups of coffee every day leads to me being ... [much less healthy|much more healthy]' or 'Drinking four cups of coffee makes me ... [very idle|very productive]'). Evaluations of expectations are always measured on a bidimensional scale.

As item stem, use 'I prefer ...', and as anchors, include the scale extremes used when measuring the expectation. For example, an item could be 'I prefer ... [being much less healthy|being much more healthy]', and another items can be 'I prefer ... [being very idle|being very productive]'. Make sure to formulate the item stem such that it makes clear that you are asking people about *their* evaluation (not what they think holds more generally). Use a seven-point response scale and try to always be consistent in the scale valence; in languages that are read from left to right, always place the most passive/low/less/weak/unlikely scale extreme (anchor) on the left, and the most active/high/more/strong/likely scale extreme (anchor) on the right. Do not reverse this order for one or more items.

The item or set of items should be accompanied by an instruction that makes clear that you are asking people about *their* evaluation (not what they think holds more generally).

####   Instrumental attitude belief expectation

Instrumental attitude belief expectations are measured using questions in a questionnaire that are referred to as 'items'. To measure an instrumental attitude belief expectation, first, identify exactly which potential instrumental consequence of the target behavior (e.g. a specific benefit or advantage) you want to measure. Then, establish whether accurately describing the spectrum of possibilities regarding this instrumental consequence requires a unidimensional scale or a bidimensional scale. Most potential consequences of a target behavior can be perceived either to *increase* or *decrease* upon performance of the target behavior, requiring a bidimensional scale. For example, some people might expect that drinking four cups of coffee every day contributes to their goal of being healthy, whether others might expect that that is unhealthy instead, expecting that not consuming any coffee at all is more healthy. However, in rare cases, one of the two dimensions can be excluded a priori, in which case a unidimensional scale suffices. For example, the degree to which people will expect that exercising regularly for a month will increase their health may vary; but is it excessively unlikely that somebody might expect that if they exercise regularly for a month, their health will *decrease*. Therefore, in that case, one might want to choose a unidimensional scale. In general, a rule of thumb is that if the 'default state' of this instrumental consequence resembles absence of the potential consequence, and therefore, engaging in the target behavior can only have an effect in one direction, a unidimensional scale can be used. However, when engaging in the target behavior can conceivably increase *or* decrease this instrumental consequence, a bidimensional scale is required.

Once it is clear whether a bidimensional or unidimensional scale should be used, the construction of the item stem and the two anchors can start. Make sure to formulate the item stem such that it makes clear that you are asking people about *their* expectation (not what they think holds more generally).

For unidimensional scales, create an item stem that explicitly lists the single dimension that expresses the instrumental consequence (e.g. exercising regularly for a month leading to increased health). As anchors, always use 'Very unlikely' and 'Very likely'. An example item would be: 'If I exercise regularly for a month, it is ... that my health will increase. [Very unlikely|Very likely]'. However, as explained above, unidimensional scales can rarely be used in most circumstances, so bidimensional scales are usually required.

For bidimensional scales, the item stem cannot explicitly list only one of the two dimensions (e.g. 'become much less healthy' or 'become much more healthy'). This is because this would create a unidimensional or ambiguous response scale. People who would score low on the unidimensional scale might mean either that they don't think that consequence will occur, or that they think that the opposite consequence will occur. For example, when creating an item 'If I drink four cups of coffee every day, I will become much more healthy. [Very unlikely|Very likely]', people who respond 'Very unlikely' can mean either that they expect to feel much less healthy, or that they expect that drinking four cups of coffee every day will have no effect on their health.

Therefore, capturing the full potential breadth of the beliefs of your target population requires asking what they expect exactly. To do this, create an item stem that contains the target behavior, and anchors that express the extremes of the bidimensional scale. For example, 'Drinking four cups of coffee every day leads to me being ... [much less healthy|much more healthy]'. Sometimes, the two extremes of the dimension you want to measure can be expressed in two antonyms, such as 'Drinking four cups of coffee makes me ... [very idle|very productive]'.

Once the item stem and the two anchors have been determined, decide which response scale to use. For bidimensional scales, seven-point scales are preferred, as these leave three degrees of expression in each dimension (the mid-point representing the expectation that the behavior does not have a consequence regarding this specific instrumental attitude belief). For unidimensional scales, five-point scales suffice. Try to always be consistent in the scale valence; in languages that are read from left to right, always place the most passive/low/less/weak/unlikely scale extreme (anchor) on the left, and the most active/high/more/strong/likely scale extreme (anchor) on the right. Do not reverse this order for one or more items.

When combining multiple items in one measurement instrument, if both evaluations of unidimensional consequences and evaluations of bidimensional consequences are measured, either use two matrices or combine them in one that uses seven-point scales for all items.

The item or set of items should be accompanied by an instruction that makes clear that you are asking people about *their* expectation (not what they think holds more generally).

####   Instrumental attitude beliefs composite

*(Not specified)*

####   Intention

Use a likert scale to ask participants to what degree they intend to, are willing to, or plan to perform the target behavior. The items suggested in the 2010 RAA book are: 'I intend to [TARGET BEHAVIOR].' with anchors 'Definitely do not' vs 'Definitely do'; 'I will [TARGET BEHAVIOR].' with anchors 'Unlikely' vs 'Likely'; 'I am willing to [TARGET BEHAVIOR].' with anchors 'False' vs 'True'; and 'I plan to [TARGET BEHAVIOR].' with anchors 'Absolutely not' vs 'Absolutely'.

####   Motivation to comply

To measure the motivation to comply with a given social referent, measure how much people want to do what the relevant social referents think they should do with regard to the target behavior.

In most behaviors and populations, unidimensional scales can be used. As item stem, use 'When it comes to [TARGET BEHAVIOR], I want to do what [SOCIAL REFERENT] think(s) I should do.', and as anchors, 'Not at all' and 'Very much'. For example, 'When it comes to whether I avoid meat during dinner, I want to do what my partner thinks I should do. ['Not at all'|'Very much']' or 'When it comes to my coffee consumption, I want to do what my colleagues think I should do. ['Not at all'|'Very much']'.

However, for some behaviors and some populations, it's not only possible that individuals want to do what a social referent thinks they should do to a certain degree, but it's also possible that the individual actively wants to do what that social referent would *disapprove* of. For example, people may strongly want to clearly demonstrate distancing themselves from members of a perceived outgroup (e.g. adolescents may want to disobey middle-aged people).

In such situations, you can use a bidimensional scale to account for this variation. Use item stem 'When it comes to [TARGET BEHAVIOR], I want to do what [SOCIAL REFERENT]...' with anchors 'Do(es)n't want me to do' and 'Want(s) me to do'. For example, 'When it comes to condom use with a partner, I want to do what other adolescents... [Don�t want me to do|Want me to do]', or 'When it comes to how late I go to bed, I want to do what my older sibling ... [Doesn�t want me to do|Wants me to do]'

####   Perceived behavioral control

Use Likert scales that measure the degree to which participants believe the target behavior to be under their control and something they are capable of performing successfully, for example by measuring their perceived capacity and perceived autonomy to perform the target behavior. The items suggested in the 2010 RAA book are: 'I am confident that if I want to, I can [TARGET BEHAVIOR].' with anchors 'No confidence at all' vs 'A lot of confidence'; 'Whether I [TARGET BEHAVIOR] is ...' with anchors 'Not up to me' vs 'Completely up to me'; 'If I really wanted to, I could [TARGET BEHAVIOR].' with anchors 'Unlikely' vs 'Likely'; and 'For me to [TARGET BEHAVIOR] is under my control.' with anchors 'Not at all' vs 'Completely'. If only the first and third of these items are measured, the 'Capacity' construct is measured instead. If only the second and fourth items are measured, the 'Autonomy' construct is measured instead. Note that questions about how easy or difficult participants perceive a target behavior to be fall outside of the constructs of Perceived Behavioral Control, Autonomy, and Capacity as defined in the 2010 RAA book (see also the discussion on page 164).

####   Perceived norms

Use likert scales that measure both perceived social approval and perceived behavior of important social referents in general. It is important that no specific individuals are referenced, but that the items do refer to individuals that are similar, important or seen as important to follow. Note that the behavior in these items must always be the target behavior. The example items in the 2010 RAA book are 'Most people who are important to me think I should [TARGET BEHAVIOR].' with anchors 'False' vs 'True'; 'Most people whose opinions I value would ... of my [TARGET BEHAVIOR]' with anchors 'Disapprove' vs 'Approve'; 'Most people I respect and admire will [TARGET BEHAVIOR].' with anchors 'Unlikely' vs 'Likely'; and 'How many people like you [TARGET BEHAVIOR]?' with anchors 'Nobody' vs 'Everybody'.

####   Perceived power of condition

The perceived power of a condition is measured by asking the extent to which a condition facilitates or impedes performance of a behavior. As the same condition can be facilitating for some people and impeding for others, this also implies that use of bidimensional scales is required. However, if one of the two dimensions can be excluded a priori, a unidimensional scale suffices.

For bidimensional scales, the items are formulated as 'If I want to [TARGET BEHAVIOR], (whether) [CONDITION] ...' with anchors 'Much harder' and 'Much easier'. For example, 'If I want to go for a run, rainy weather makes it [Much harder|Much easier].'

For unidimensional scales, which are only used if one of the dimensions can be excluded a priori, the way the items are formulated depends on whether they concern a barrier or a facilitator. For facilitators, use 'If I want to [TARGET BEHAVIOR], whether [CONDITION]...' with anchors 'Doesn't matter' and 'Makes it much easier', and for barriers, 'If I want to [TARGET BEHAVIOR], whether [CONDITION]...' with anchors 'Doesn't matter' and 'Makes it much harder'.

For example, 'If I want to use anticonception, whether it is widely available...' with anchors 'Doesn't matter' to 'Makes it much easier'.

####   Perceived presence of condition

The perceived presence of a condition is measured by asking the perceived probability that a condition will be present. A unidimensional scale is required. The items are formulated as 'How likely do you think it is that [CONDITION]?' with anchors 'Very unlikely' and 'Very likely'.

For example, 'How likely do you think it is that it will rain?' and 'How likely do you think it is that anticonception will be widely available?', both with anchors 'Very unlikely' and 'Very likely'.

####   Perceived referent approval

Use item stems that list the target behavior and the social referent, and use disapproval and approval as anchors, in that order, with an intensifying adjective. Specifically, as item stem, use 'If I were to (engage in) [TARGET BEHAVIOR], [SOCIAL REFERENT] would...' with anchors 'strongly disapprove' versus 'strongly approve'. Note that the 'engage in' is optional, depending on the kind of behavior. For example, 'If I were to engage in a demonstration, my neighbour would... [Strongly disapprove|Strongly approve]'

Because this is a bidimensional scale, the scale midpoint reflects neutrality (i.e. one does not think the relevant social referent disapproves or approves of this target behavior. This means that a seven-point response scale is slightly preferred over a five-point response scale to allow participants to express three degrees of perceived (dis)approval.

####   Perceived referent behavior

To measure the perceptions individuals have of social referents' behavior, measure the probability that they engage in that target behavior. Use an item stem listing both the target behavior and the social referent, and use anchors 'improbable' versus 'probable', with intensifying adjectives.

'How likely do you think it is that [SOCIAL REFERENT] engage(s) in [TARGET BEHAVIOR]?' with anchors 'Very improbable' and 'Very probable'.

For example, 'How likely do you think it is that your close family members engage in recycling? [Very improbable|Very probable]' 

####   Perceived subskill importance

The perceived importance of a subskill is measured by asking the extent to which possessing the subskill is relevant to performance of the behavior. The items are formulated as 'For me to successfully [BEHAVIOR], being able to [SUBSKILL] is...' with 'Not at all important' and 'extremely important' as anchors.

For example, 'For me to successfully use condoms, being able to ask for condoms at the counter is [not at all important|extremely important],' 'For me to successfully moderate my alcohol intake, being able to decline an alcoholic drink when it is offered is [not at all important|extremely important].'

####   Perceived subskill presence

The perceived presence of a subskill is measured by asking the perceived probability that a person has a certain skill that is required to successfully accomplish the target behavior. It is important that the item stem explicitly describes a scenario where not successfully accomplishing the relevant sub-behavior (that corresponds to the relevant subskill) can only be the consequence of insufficiently possessing that subskill.

In the item stem, start with a word that establishes that the question concerns a regular situation. Then, if the subskill pertains to a specific scenario, insert that condition. Then continue with 'are you able to [TARGET BEHAVIOR]'. For subskills that do not pertain to specific scenarios, the item stem then becomes 'Typically, are you able to [TARGET BEHAVIOR]', and for subskills that do pertain to a specific scenario, the item stem becomes 'Typically, if [SCENARIO], are you able to [TARGET BEHAVIOR]'.

As anchors, use 'Absolutely unable' to 'Absolutely able' (this is a unidimensional scale).

For example, 'Typically, are you able to ask for condoms at the counter?' and 'Typically, if an alcoholic drink is offered, are you able to decline?', both with anchors 'Absolutely unable' and 'Absolutely able'.

####   Target behavior

Depending on the nature of the target behavior, engagement in that behavior can be conceived as binary (i.e. one either does or does not engage in the target behavior, e.g. getting tested for STIs every six months), a matter of frequency (i.e. one engages in the target behavior with a frequency from zero up to a given feasible maximum frequency in a given timespan, e.g. the frequency with which one washes their hands conform the guidelines), a matter of intensity (i.e. one engages in the target behavior to a degree from zero up to a given feasible maximum intensity, e.g. the amount of kilocalories one consumed in a meal), or a combination of these (i.e. one engages in the target behavior with a given frequency and with a given intensity, e.g. how frequently one drinks alcohol, and how many grams of alcohol one consumes when one does).

