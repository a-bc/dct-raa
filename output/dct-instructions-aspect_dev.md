### Instruction for developing aspects
*This overview was generated on 2020-04-10 at 16:21:29 CEST (GMT+0200)*

####   Actual control

*(Not specified)*

####   Attitude

*(Not specified)*

####   Autonomy

*(Not specified)*

####   Autonomy belief

*(Not specified)*

####   Autonomy beliefs composite

*(Not specified)*

####   Capacity

Conduct a qualitative study where participants are interviewed, and the interviews are either recorded and transcribed, or notes are kept. These sources are then coded using the instruction for aspect coding. In this qualitative study, use these two questions. 'Please list any factors or circumstances that would make it easy or enable you to [target behavior].' and 'Please list any factors or circumstances that would make it difficult or prevent you from [target behavior].'

####   Capacity belief

*(Not specified)*

####   Capacity beliefs composite

*(Not specified)*

####   Descriptive norm belief

*(Not specified)*

####   Descriptive norms

Conduct a qualitative study where participants are interviewed, and the interviews are either recorded and transcribed, or notes are kept. These sources are then coded using the instruction for aspect coding. In this qualitative study, use the introduction 'Sometimes, when we are not sure what to do, we look to see what others are doing.', and then ask these questions: 'Please list the individuals or groups who are most likely to [TARGET BEHAVIOR].' and 'Please list the individuals or groups who are least likely to [TARGET BEHAVIOR].'.

####   Descriptive norms belief composite

*(Not specified)*

####   Experiential attitude

Experiential attitude is defined as a construct that is the consequence of a person's evaluation of the experiential attitude belief composite. 

####   Experiential attitude belief

*(Not specified)*

####   Experiential attitude belief evaluation

Conduct a qualitative study where participants are interviewed, and the interviews are either recorded and transcribed, or notes are kept. These sources (i.e. transcripts or notes) are then coded using the instruction for aspect coding. In this qualitative study, use these questions: 'What do you see as the advantages of you engaging in [target behavior]?', 'What do you see as the disadvantages of you engaging in [target behavior]?', and 'What else comes to mind when you think about [target behavior]?'.

####   Experiential attitude belief expectation

Conduct a qualitative study where participants are interviewed, and the interviews are either recorded and transcribed, or notes are kept. These sources (i.e. transcripts or notes) are then coded using the instruction for aspect coding. In this qualitative study, use these questions: 'What do you see as the advantages of you engaging in [target behavior]?', 'What do you see as the disadvantages of you engaging in [target behavior]?', and 'What else comes to mind when you think about [target behavior]?'.

####   Experiential attitude beliefs composite

*(Not specified)*

####   Identification with referent

*(Not specified)*

####   Injunctive norm belief

*(Not specified)*

####   Injunctive norm beliefs composite

*(Not specified)*

####   Injunctive norms

*(Not specified)*

####   Instrumental attitude

*(Not specified)*

####   Instrumental attitude belief

*(Not specified)*

####   Instrumental attitude belief evaluation

Conduct a qualitative study where participants are interviewed, and the interviews are either recorded and transcribed, or notes are kept. These sources (i.e. transcripts or notes) are then coded using the instruction for aspect coding. In this qualitative study, use these questions: 'What do you see as the advantages of you engaging in [target behavior]?', 'What do you see as the disadvantages of you engaging in [target behavior]?', and 'What else comes to mind when you think about [target behavior]?'.

####   Instrumental attitude belief expectation

Conduct a qualitative study where participants are interviewed, and the interviews are either recorded and transcribed, or notes are kept. These sources (i.e. transcripts or notes) are then coded using the instruction for aspect coding. In this qualitative study, use these questions: 'What do you see as the advantages of you engaging in [target behavior]?', 'What do you see as the disadvantages of you engaging in [target behavior]?', and 'What else comes to mind when you think about [target behavior]?'.

####   Instrumental attitude beliefs composite

*(Not specified)*

####   Intention

*(Not specified)*

####   Motivation to comply

*(Not specified)*

####   Perceived behavioral control

Conduct a qualitative study where participants are interviewed, and the interviews are either recorded and transcribed, or notes are kept. These sources are then coded using the instruction for aspect coding. In this qualitative study, use these questions: 'Please list any factors or circumstances that would make it easy or enable you to [target behavior].' and 'Please list any factors or circumstances that would make it difficult or prevent you from [target behavior].'

####   Perceived norms

Conduct a qualitative study where participants are interviewed, and the interviews are either recorded and transcribed, or notes are kept. These sources are then coded using the instruction for aspect coding. In this qualitative study, use the introduction 'When it comes to [TARGET BEHAVIOR], there might be individuals or groups who would think you should or should not perform this behavior.', and then ask these questions: 'Please list any individuals or groups who would approve or think you should [TARGET BEHAVIOR].'; 'Please list any individuals or groups who would disapprove or think you should not [TARGET BEHAVIOR].'; 'Sometimes, when we are not sure what to do, we look to see what others are doing. Please list the individuals or groups who are most likely to [TARGET BEHAVIOR].'; and 'Please list the individuals or groups who are least likely to [TARGET BEHAVIOR].'.

####   Perceived power of condition

*(Not specified)*

####   Perceived presence of condition

*(Not specified)*

####   Perceived referent approval

Page 135 of the RAA book

####   Perceived referent behavior

*(Not specified)*

####   Perceived subskill importance

*(Not specified)*

####   Perceived subskill presence

*(Not specified)*

####   Target behavior

*(Not specified)*

