### Instruction for coding measurement instruments
*This overview was generated on 2020-04-10 at 16:21:29 CEST (GMT+0200)*

####   Actual control

*(Not specified)*

####   Attitude

Attitude consists of two sub-constructs, and is measured by measuring those: [dct:experientialAttitude_73dnt5z5](#experientialAttitude_73dnt5z5) and [dct:instrumentalAttitude_73dnt5zb](#instrumentalAttitude_73dnt5zb).

####   Autonomy

*(Not specified)*

####   Autonomy belief

*(Not specified)*

####   Autonomy beliefs composite

*(Not specified)*

####   Capacity

Questions or questionnaires that measure the perceived degree to which [target behavior] or [contrast behavior] is something [target population] are confident they can successfully perform. If perceived control over the behavior is also measured, code this as [dct:perceivedBehavioralControl_71w8sfdk](#perceivedBehavioralControl_71w8sfdk). For example, the items suggested in the book are: 'I am confident that if I want to, I can [TARGET BEHAVIOR].' with anchors 'No confidence at all' vs 'A lot of confidence' and 'If I really wanted to, I could [TARGET BEHAVIOR].' with anchors 'Unlikely' vs 'Likely'.

####   Capacity belief

*(Not specified)*

####   Capacity beliefs composite

*(Not specified)*

####   Descriptive norm belief

*(Not specified)*

####   Descriptive norms

*(Not specified)*

####   Descriptive norms belief composite

*(Not specified)*

####   Experiential attitude

Operationalisations that measure affective aspects of the latent disposition or tendency to respond favourably versus unfavourably to [target behavior], for example using the semantic differentials 'pleasant' vs 'unpleasant' or 'fun' vs 'boring'.

####   Experiential attitude belief

*(Not specified)*

####   Experiential attitude belief evaluation

Experiential attitude belief evaluations are measured using questions in a questionnaire (these are referred to as 'items'). Items can be coded as measuring an experiential attitude belief evaluation if they measure participants' evaluation of one specific experiential potential consequence of a behavior as positive or negative (i.e. desirable or undesirable).

The experiential nature of this evaluation means that this concerns mostly expected experiences and sensations, such as pleasure or pain.

The question should include, either in its stem or in its anchors, the specific dimension of which the evaluation is being measured (e.g. whether people prefer feeling relaxed or feeling excited, or whether people evaluate being excited as positive or negative).

Note that evaluations of consequences that render the target behavior less or more instrumental given more long-term goals are captured in instrumental attitude and the underlying beliefs. (see [dct:expAttitude_expectation_73dnt5z1](#expAttitude_expectation_73dnt5z1) and [dct:instrAttitude_expectation_73dnt5z6](#instrAttitude_expectation_73dnt5z6) for more details about this distinction, and see [dct:instrAttitude_evaluation_73dnt5z7](#instrAttitude_evaluation_73dnt5z7) for the coding instruction for instrumental attitude belief evaluations).

####   Experiential attitude belief expectation

Experiential attitude belief expectations are measured using questions in a questionnaire (these are referred to as 'items'). Items can be coded as measuring an experiential attitude belief expectation if they measure participants' expectation of how probable (i.e. unlikely versus likely) it is that engaging in the target behavior will cause one specific experiential potential consequence to come about. The experiential nature of this expectation means that these expected consequences must concern experiences and sensations, such as pleasure or pain.

These experiential attitude belief expectations cover acute hedonic expectations, disconnected from potential long-term consequences the target behavior may have. Experiential attitude belief expectations always refer to immediately experienced consequences of a behavior.

The question should include, either in its stem or in its anchors, the specific expectation being measured.

Note that items concerning consequences that render the target behavior less or more desirable without the immediate experiential effects of the behavior playing a role are captured in instrumental attitude belief expectations (see [dct:instrAttitude_expectation_73dnt5z6](#instrAttitude_expectation_73dnt5z6)).

####   Experiential attitude beliefs composite

*(Not specified)*

####   Identification with referent

*(Not specified)*

####   Injunctive norm belief

*(Not specified)*

####   Injunctive norm beliefs composite

*(Not specified)*

####   Injunctive norms

*(Not specified)*

####   Instrumental attitude

*(Not specified)*

####   Instrumental attitude belief

*(Not specified)*

####   Instrumental attitude belief evaluation

Instrumental attitude belief evaluations are measured using questions in a questionnaire (these are referred to as 'items'). Items can be coded as measuring an instrumental attitude belief evaluation if they measure participants' evaluation of one specific instrumental potential consequence of a behavior as positive or negative (i.e. desirable or undesirable).

The instrumental nature of this evaluation means that this these evaluations contain information about people's longer term goals.

The question should include, either in its stem or in its anchors, the specific dimension of which the evaluation is being measured (e.g. whether people prefer being unhealthy versus healthy, or whether people evaluate being healthy as positive or negative).

Note that evaluations of consequences that concern experiences and sensations, such as pleasure or pain, are captured in experiential attitude and the underlying beliefs (see [dct:instrAttitude_expectation_73dnt5z6](#instrAttitude_expectation_73dnt5z6) and [dct:expAttitude_expectation_73dnt5z1](#expAttitude_expectation_73dnt5z1) for more details about this distinction, and see [[dct::expAttitude_evaluation_73dnt5z2]] for the coding instruction for experiential attitude belief evaluations).

####   Instrumental attitude belief expectation

Instrumental attitude belief expectations are measured using questions in a questionnaire (these are referred to as 'items'). Items can be coded as measuring an instrumental attitude belief expectation if they measure participants' expectation of how probable (i.e. unlikely versus likely) it is that engaging in the target behavior will cause one specific instrumental potential consequence to come about. The instrumental nature of this consequence denotes that instrumental attitude belief expectations must concern consequences that facilitate or hinder achieving one or more longer term goals.

These instrumental attitude belief expectations cover long-term consequences, disconnected from acute hedonic consequences the target behavior may have. Instrumental attitude belief expectations always refer to consequences that cause a behavior to contribute more or less to goals an individual has.

The question should include, either in its stem or in its anchors, the specific expectation being measured.

Note that items concerning experiencing the consequences of a behavior, for example expectations relating to experiences and sensations, are captured in experiential attitude belief expectations (see [dct:expAttitude_expectation_73dnt5z1](#expAttitude_expectation_73dnt5z1)).

####   Instrumental attitude beliefs composite

*(Not specified)*

####   Intention

Operationalisations that measure the degree to which a target population member has a deliberate (reasoned) plan/intention to engage in [TARGET BEHAVIOR]. For example, the items suggested in the book are: 'I intend to [TARGET BEHAVIOR].' with anchors 'Definitely do not' vs 'Definitely do'; 'I will [TARGET BEHAVIOR].' with anchors 'Unlikely' vs 'Likely'; 'I am willing to [TARGET BEHAVIOR].' with anchors 'False' vs 'True'; and 'I plan to [TARGET BEHAVIOR].' with anchors 'Absolutely not' vs 'Absolutely'.

####   Motivation to comply

*(Not specified)*

####   Perceived behavioral control

Questions or questionnaires that measure the perceived degree to which the target behavior or the contrast behavior is both under the control of the target population individual and something they are confident they can successfully perform. If only one aspect is measured (i.e. only control or only confidence), code this as the 'Autonomy' or 'Capacity' constructs. For example, the items suggested in the book are: 'I am confident that if I want to, I can [TARGET BEHAVIOR].' with anchors 'No confidence at all' vs 'A lot of confidence'; 'Whether I [TARGET BEHAVIOR] is ...' with anchors 'Not up to me' vs 'Completely up to me'; 'If I really wanted to, I could [TARGET BEHAVIOR].' with anchors 'Unlikely' vs 'Likely'; and 'For me to [TARGET BEHAVIOR] is under my control.' with anchors 'Not at all' vs 'Completely'. If only the first and third of these items are measured, the 'Capacity' construct should be coded instead. If only the second and fourth items are measured, the 'Autonomy' construct should be coded instead.

####   Perceived norms

Questionnaires that measure both perceived social approval and perceived behavior of important social referents in general. It is important that no specific individuals are referenced, but that the items do refer to individuals that are similar, important or seen as important to follow. Note that the behavior in these items must always be the target behavior.

####   Perceived power of condition

*(Not specified)*

####   Perceived presence of condition

*(Not specified)*

####   Perceived referent approval

*(Not specified)*

####   Perceived referent behavior

*(Not specified)*

####   Perceived subskill importance

*(Not specified)*

####   Perceived subskill presence

*(Not specified)*

####   Target behavior

*(Not specified)*

