---
dct:
  version: 0.1.0
  id: descriptiveNorms_73dnt5zp
  label: "Descriptive norms"
  date: 2019-04-17
  source:
    label: "Fishbein, M. & Ajzen, I. (2010) Predicting and Changing Behavior: The Reasoned Action Approach. New York: Psychology Press."
    xdoi: "isbn:9781138995215"
  definition:
    definition:
################################################################################
  measure_dev:
    instruction: "Use likert scales that measure perceived behavior of important social referents in general. It is important that no specific individuals are referenced, but that the items do refer to individuals that are similar, important or seen as important to follow. Note that the behavior in these items must always be the target behavior. The example items in the 2010 RAA book are 'Most people I respect and admire will [TARGET BEHAVIOR].' with anchors 'Unlikely' vs 'Likely'; and 'How many people like you [TARGET BEHAVIOR]?' with anchors 'Nobody' vs 'Everybody'."
################################################################################
  measure_code: 
    instruction: 
################################################################################
  manipulate_dev:
    instruction: These instructions still have to be extracted from pages 336-359 of the book.
    source:
      spec: "p. 336-359"
################################################################################
  manipulate_code:
    instruction:
################################################################################
  aspect_dev:
    instruction: "Conduct a qualitative study where participants are interviewed, and the interviews are either recorded and transcribed, or notes are kept. These sources are then coded using the instruction for aspect coding. In this qualitative study, use the introduction 'Sometimes, when we are not sure what to do, we look to see what others are doing.', and then ask these questions: 'Please list the individuals or groups who are most likely to [TARGET BEHAVIOR].' and 'Please list the individuals or groups who are least likely to [TARGET BEHAVIOR].'."
    source:
      spec: "pp. 451-452"
################################################################################
  aspect_code:
    instruction: "Expressions of perceived behavior of others, i.e. whether others perform the target behavior or not. Note that exressions of perceived *approval or disapproval* should be coded as dct:perceivedNorm_injunctive_73bg2wm7, and expressions of perceived norms where it is unclear whether they concern perceived (dis)approval or perceived approval should be coded as dct:perceivedNorm_71w98kk2."
################################################################################

  rel:
    id: "perceivedNorms_73dnt5zq"
    type: "structural_part_of"

################################################################################
---

