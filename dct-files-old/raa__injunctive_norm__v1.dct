---
dct:
  -
    id: perceivedNorm_injunctive_73bg2wm7
    label: Injunctive norm
    date: 2019-04-17
    source:
      label: "Fishbein, M. & Ajzen, I. (2010) Predicting and Changing Behavior: The Reasoned Action Approach. New York: Psychology Press."
      xdoi: "isbn:9781138995215"
    definition:
      definition: Injunctive norm Def
      id: pp. 17-18
################################################################################
    measure_dev:
      instruction: "Use likert scales that measure both perceived social approval and perceived behavior of important social referents in general. It is important that no specific individuals are referenced, but that the items do refer to individuals that are similar, important or seen as important to follow. Note that the behavior in these items must always be the target behavior. The example items in the 2010 RAA book are 'Most people who are important to me think I should [TARGET BEHAVIOR].' with anchors 'False' vs 'True'; 'Most people whose opinions I value would ... of my [TARGET BEHAVIOR]' with anchors 'Disapprove' vs 'Approve'; 'Most people I respect and admire will [TARGET BEHAVIOR].' with anchors 'Unlikely' vs 'Likely'; and 'How many people like you [TARGET BEHAVIOR]?' with anchors 'Nobody' vs 'Everybody'."
################################################################################
    measure_code:
      instruction: STILL HAS TO BE ADDED!
################################################################################
    manipulate_dev:
      instruction: These instructions still have to be extracted from pages 336-359 of the book.
      source:
        spec: "p. 336-359"
################################################################################
    manipulate_code:
      instruction:
################################################################################
    aspect_dev:
      instruction: "Conduct a qualitative study where participants are interviewed, and the interviews are either recorded and transcribed, or notes are kept. These sources are then coded using the instruction for aspect coding. In this qualitative study, use the introduction 'When it comes to [TARGET BEHAVIOR], there might be individuals or groups who would think you should or should not perform this behavior.', and then ask these questions: 'Please list any individuals or groups who would approve or think you should [target behavior].' and 'Please list any individuals or groups who would disapprove or think you should not [TARGET BEHAVIOR].'."
      source:
        spec: "pp. 451-452"
################################################################################
    aspect_code:
      instruction: "Expressions of perceived approval or disapproval, or even social pressure, to engage (or not engage) in the target behavior. Note that expressions of perceived *behavior* of others should be coded as [dct:perceivedNorm_descriptive_73bg61tx], and expressions of perceived norms where it is unclear whether they concern perceived (dis)approval or perceived approval should be coded as [dct:perceivedNorm_71w98kk2]."
################################################################################
    rel:
      -
        id: perceivedNorm_71w98kk2
        type: causal_influences_unspecified
---
